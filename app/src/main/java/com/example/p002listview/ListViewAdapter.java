package com.example.p002listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewAdapter extends ArrayAdapter<ItemData> {
    private int groupId;
    private ArrayList<ItemData> list;
    private LayoutInflater inflater;

    public ListViewAdapter(Context context, int groupId, ArrayList<ItemData> list) {
        super(context, groupId, list);
        this.list = list;
        inflater = LayoutInflater.from(context);
        this.groupId = groupId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(R.layout.listview_layout, parent, false);
        ImageView image = itemView.findViewById(R.id.imgCategoria);
        image.setImageResource(list.get(position).getImageId());
        TextView textCategoria = itemView.findViewById(R.id.lblCategorias);
        textCategoria.setText(list.get(position).getTextCategoria());
        TextView textDescripcion = itemView.findViewById(R.id.lblDescripcion);
        textDescripcion.setText(list.get(position).getTextDescripcion());
        return itemView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}
